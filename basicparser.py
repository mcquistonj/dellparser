#!/usr/bin/python
from testresult import *
from datetime import datetime, timedelta, date
from os import rename
from sys import stdout
import glob
from math import sqrt

class BasicParser():
    def parse(self, filenames, debug=0, configs=None):
        self.DEBUG=debug
        perfs = self.genPerfFiltered(filenames, debug, configs)
        # self.rename(filenames)
        return perfs

    def rename(self, filenames, dryrun=True):
        rename=[]
        script=''
        for filename in filenames:
            with open(filename) as log:
                for line in log:
                    if line.strip().endswith('Test Description'):
                        line=log.next()
                        script = line[line.find(',')+1:]
                        break
            datestr='%m%d%y'
            td=date.today()
            cap='{CAP}'
            sn='{SN}'
            fw='{FW}'            
            newname='D2D_Intel_SSD_{CAP}_{SN}_{FW}_{script}_{mmddyy}.csv'.format(CAP=cap, SN=sn, FW=fw, script=script, mmddyy=td.strftime(datestr))
            prefix=filename[:max(filename.rfind('/'),filename.rfind('\\'))+1]
            rename.append((prefix,filename,newname))
        if dryrun:
            if self.DEBUG==2:
                print 'Renaming (dryrun):\n'
                for prefix,old,new in rename:
                    print old, '\n -> \n', prefix+new
                    print
        else:
            for prefix, old, new in rename:
                os.rename(old,prefix+new)
 
    def genPerfFiltered(self, filenames, debug=0, configs=None):        
        tests = []
        if self.DEBUG==1: self.loops=0
        if configs and 'iometer' in configs.keys():
            filenames=self.flatten(map(glob.glob, configs['iometer'].values()))
        datalocs=[44, 2, 6, 9, 14, 19, 24, 45]
        filenumber=1
        for filename in filenames:
            test = ParsedTest()

            test.name = filename[-31:]

            filenumber+=1
            test.files=[filename]
            test.data=[['Queue Depth', 'Access Specification Name', 'IOps', 'MBps', 'Average Response Time', 'Maximum Response Time', 
                'Errors', '% CPU Utilization']]
            with open(filename) as log:
                for line in log:
                    if line.startswith("'Time Stamp"):
                        line=log.next()
                        starttime = datetime.strptime(line.replace(',','').strip()[:-4], "%Y-%m-%d %H:%M:%S")
                    elif line.startswith("'Target Type"):
                        header=map(lambda x:x.strip(),line[1:].upper().split(','))
                        QDloc = header.index('QUEUE DEPTH')
                        ASNloc = header.index('ACCESS SPECIFICATION NAME')
                        IOPSloc = header.index('IOPS')
                        MBPSloc = header.index('MBPS')
                        ARTloc = header.index('AVERAGE RESPONSE TIME')
                        MRTloc = header.index('MAXIMUM RESPONSE TIME')
                        ERRloc = header.index('ERRORS')
                        CPUUloc = header.index('% CPU UTILIZATION')
                        datalocs = [QDloc, ASNloc, IOPSloc, MBPSloc, ARTloc, MRTloc, ERRloc, CPUUloc]
                        line=log.next()
                        vals=line.split(',')
                        if len(vals) > max(datalocs):
                            row=[]
                            shouldappend=True
                            for i in datalocs:
                                c=vals[i].strip()
                                if c:
                                    row.append(c)
                                else:
                                    shouldappend=False
                                    break
                            if shouldappend:
                                try:
                                    if int(row[0])<int(test.data[-1][0]):
                                        test.data.append(['','','','','','','',''])
                                except:
                                    pass
                                test.data.append(row)

            tests.append(test)
            if self.DEBUG==1: 
                self.loops+=1
                stdout.write("\r"+'.'*self.loops)
                stdout.flush()
        if self.DEBUG==1: print ''
        return tests

    def flatten(self,l):
        ret=[]
        for i in l:
            if isinstance(i,list):
                ret+=self.flatten(i)
            else:
                ret.append(i)
        return ret
