#!/usr/bin/env python
"""Creates xls file from a testresult object"""
from testresult import *
from xlsxwriter.workbook import Workbook

# https://xlsxwriter.readthedocs.org/en/latest/working_with_cell_notation.html

# https://xlsxwriter.readthedocs.org/en/latest/working_with_charts.html
# https://xlsxwriter.readthedocs.org/en/latest/examples.html
# https://xlsxwriter.readthedocs.org/en/latest/example_conditional_format.html
# https://xlsxwriter.readthedocs.org/en/latest/example_images.html
# https://xlsxwriter.readthedocs.org/en/latest/working_with_formats.html
# https://xlsxwriter.readthedocs.org/en/latest/working_with_conditional_formats.html
class SixtyThreeHourPublisher():
    def publish(self, parsedOutput, outputfile='D2D_Compare.xlsx', debug=0, 
                argsdict={}):
        self.DEBUG=debug
        if outputfile.endswith('.xlsx'):
            try: os.remove(outputfile)
            except: pass
            workbook = Workbook(outputfile)
        elif outputfile.endswith('.xls'):
            outputfile=outputfile+'x'
            try: os.remove(outputfile)
            except: pass
            workbook = Workbook(outputfile)
        elif outputfile.endswith('.XLSX'):
            try: os.remove(outputfile)
            except: pass
            workbook = Workbook(outputfile)
        elif outputfile.endswith('.XLS'):
            outputfile=outputfile+'X'
            try: os.remove(outputfile)
            except: pass
            workbook = Workbook(outputfile)
        else:
            try: os.remove(outputfile+'.xlsx')
            except: pass
            workbook = Workbook(outputfile+'.xlsx')

        failedstyle1 = workbook.add_format({'bg_color': '#993300'})
        failedstyle2 = workbook.add_format({'bg_color': '#FFCC00'})
        failedstyle3 = workbook.add_format({'bg_color': '#FFFF99'})

        for p in parsedOutput:
            worksheet = workbook.add_worksheet(p.name)
            for y,row in enumerate(p.data):
                for x, val in enumerate(row+[' ']):
                    dat=val[val.find(':')+1:]
                    if val.startswith('FAILEDLEVEL1:'):
                        worksheet.write(y, x, dat, failedstyle3)
                    elif val.startswith('FAILEDLEVEL2:'):
                        worksheet.write(y, x, dat, failedstyle2)
                    elif val.startswith('FAILEDLEVEL3:'):
                        worksheet.write(y, x, dat, failedstyle1)
                    else:
                        worksheet.write(y, x, dat)
        workbook.close()
