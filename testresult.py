#!/usr/bin/python

class ParsedTest:
    testName = ''
    message = ''
    files = []
    data = []
    graph=None

    def debugoutput(self):
        print 'Name: ' + str(self.name)
        print 'Message: ' + str(self.message)
        print 'Files: ' + str(self.files)        
        print 'Data: '
        for dat in self.data:
            print '\t' + (str(dat))
        print 'Graph: ' + str(self.graph)