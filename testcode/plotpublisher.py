# #!/usr/bin/env python

from matplotlib import pyplot
# import matplotlib.pyplot as pyplot
import random
# from numpy import arange

class  PlotPublisher():
    host=None
    lines=[]
    xmin=None
    xmax=None
    ymin=None
    ymax=None

    def __init__(self, xlabel='', ylabel='', title=''):
        fig = pyplot.figure()
        pyplot.title(title)
        fig.subplots_adjust(right=0.75)
        self.host = fig.add_subplot(111)
        self.host.set_xlabel(xlabel)

        self.host.set_ylabel(ylabel)

    def addline(self, data, marker='', label='', color='black', First=True):
        try:
            data=sorted(data)
        except:
            pass
        x=map(lambda xx:xx[0],data)
        y=map(lambda xx:xx[1],data)

        if all(map(lambda xx:isinstance(xx,str), x)):
            newx=map(lambda xx:xx+.5,range(len(x)))
            if First:
	            pyplot.xticks(newx, tuple(map(str,x)), rotation='vertical')
            x=newx

        if all(map(lambda xx:isinstance(xx,str), y)):
            newy=map(lambda xx:xx+.5,range(len(y)))
            pyplot.yticks(newy, tuple(map(str,y)))
            y=newy

        p = self.host.plot(x, y, 'o-', marker=marker, label=label, color=color)[0]
        self.lines.append(p)

        try:
            if self.xmax==None:
                self.xmax=max(x)+.1*abs(max(x))
            elif max(x) > self.xmax:
                self.xmax=max(x)+.1*abs(max(x))

            if self.xmin==None:
                self.xmin=min(x)+.1*abs(min(x))
            if min(x) < self.xmin:
                self.xmin=min(x)-.1*abs(min(x))

            if self.ymax==None:
                self.ymax=max(y)+.1*abs(max(y))
            if max(y) > self.ymax:
                self.ymax=max(y)+.1*abs(max(y))

            if self.ymin==None:
                self.ymin=min(y)+.1*abs(min(y))
            if min(y) < self.ymin:
                self.ymin=min(y)-.1*abs(min(y))
        except:
            pass

        self.host.set_xlim(self.xmin, self.xmax)
        self.host.set_ylim(self.ymin, self.ymax)
        self.host.legend(self.lines, map(lambda x:x.get_label(), self.lines))
        return p

    def show(self):
        pyplot.show()

    def publish(self, parsedOutput, outputfile='', debug=0, argsdict={}):
        self.DEBUG=debug
        # pub=PlotPublisher(xlabel=xlabel, ylabel=ylabel, title=Name)
        
        f=True
        for p in parsedOutput:
            if p.name.startswith('PerfFilterd'):
                ASloc = p.data[0].index('Access Specification Name')
                IOloc = p.data[0].index('IOps')
                dat=map(lambda x:[x[ASloc],float(x[IOloc]),],filter(lambda x: x[0]!='',p.data[1:]))
                
                self.addline(dat, marker=self.getrandommarker(), label=p.name,color=self.getrandomcolor(), First=f)
                f=False
        self.show()

    def getrandommarker(self):
        markers= ['0', '1', '2', '3', '4', 'D', '6', '7', 's', '|', 'x', '5', '_', '^', 'd', 'h', '+', '*', ',', 'o', '.', '1', 'p', '3', '2', '4', 'H', 'v', '8', '<', '>', r'$\lambda$', r'$\bowtie$', r'$\circlearrowleft$',r'$\clubsuit$',r'$\checkmarks$']
        ret =markers[random.randrange(len(markers))]
        return ret

    def getrandomcolor(self):
        colors = ['red','green','blue','pink','black']
        return colors[random.randrange(len(colors))]
