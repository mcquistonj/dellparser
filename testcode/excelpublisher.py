#!/usr/bin/env python
"""Creates xls file from a testresult object"""
from testresult import *
import xlwt
# import xlrd

class ExcelPublisher():
    def publish(self, parsedOutput, outputfile='D2D_Compare.xls', debug=0, argsdict={}):
        self.DEBUG=debug
        if not outputfile.endswith('.xls'):
            outputfile = outputfile+'.xls'
        
        workbook = xlwt.Workbook()

        # workbook.set_colourRGB("lev1", 36, 255, 255, 153)
        # workbook.set_colourRGB("lev2", 44, 255, 204, 0)
        # workbook.set_colourRGB("lev3", 53, 153, 51, 0)
        # xlwt.add_palette_colour("lev1", 36)
        # xlwt.add_palette_colour("lev2", 44)
        # xlwt.add_palette_colour("lev3", 53)

        stylestring='pattern: pattern solid, pattern_fore_colour {color}, pattern_back_colour {color}'
        # stylestring=''#colorindex : {color}'#$pattern: pattern solid, pattern_fore_colour {color}, pattern_back_colour {color}'
        failedstyle1=xlwt.easyxf(stylestring.format(color='36'))#'&FFFF99'))#RGB(255,255,153)'))
        failedstyle2=xlwt.easyxf(stylestring.format(color='44'))#'&FFCC00'))#RGB(255,204,0)'))
        failedstyle3=xlwt.easyxf(stylestring.format(color='53'))#'&993366'))#RGB(153,51,0)'))

        for p in parsedOutput:
            worksheet = workbook.add_sheet(p.name)
            for y,row in enumerate(p.data):
                for x, val in enumerate(row):
                    try:
                        if val.startswith('FAILEDLEVEL1:'):
                            worksheet.write(y,x,val[13:],failedstyle1)
                        if val.startswith('FAILEDLEVEL2:'):
                            worksheet.write(y,x,val[13:],failedstyle2)
                        if val.startswith('FAILEDLEVEL3:'):
                            worksheet.write(y,x,val[13:],failedstyle3)
                        else:
                            worksheet.write(y,x,val)
                    except Exception as e:
                        # workbook.save(outputfile)
                        if self.DEBUG==2:
                            print 'problem at ({x},{y}):{v}'.format(x=x,y=y,v=val), '\n\t',e
                            # if not val.startswith('FAILEDLEVEL'):
                                # print x,y,val
                            # print y,x,row
        try: os.remove(outputfile)
        except: pass
        workbook.save(outputfile)
        
