#!/usr/bin/python

import sys
from PySide import QtGui

class Example(QtGui.QWidget):
    def __init__(self,app):
        super(Example, self).__init__()
        caption = 'Open file'
        directory = './'
        filter_mask = "Comma Seperated Value sheets (*.csv)"
        filenames = QtGui.QFileDialog.getOpenFileNames(None, caption, directory, filter_mask)[0]
        for f in filenames:
            print(f)  
        text, ok = QtGui.QInputDialog.getText(self, 'Input Dialog', 
            'Enter in the desired name for the output file:')
        if ok:
            print text
        sys.exit()

def main():
    app = QtGui.QApplication([])
    ex = Example(app)

if __name__ == '__main__':
    main()
