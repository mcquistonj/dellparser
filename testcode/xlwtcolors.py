#!/usr/bin/env python
import xlwt

# http://dmcritchie.mvps.org/excel/colors.htm
# http://msdn.microsoft.com/en-us/library/office/cc296089(v=office.12).aspx
workbook = xlwt.Workbook()
stylestring='pattern: pattern solid, pattern_fore_colour {color}, pattern_back_colour {color}'
worksheet = workbook.add_sheet('test')

for i in xrange(100):
    try:
        sty = xlwt.easyxf(stylestring.format(color=i))
        worksheet.write(i, 1, 'SAMPLE TEXT', sty)
    except Exception as e:
            print i,e
            workbook.save('sample.xls')
            break
workbook.save('sample.xls')

