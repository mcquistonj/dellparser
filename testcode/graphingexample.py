from iometerlogparser import IometerLogParser

# http://matplotlib.org/1.2.1/examples/pylab_examples/barh_demo.html

io=IometerLogParser()
fis=['WL_800GB/D2D_INTEL_SSD_800GB_BTWL305002M2800JGN_DL10_SSD_StandardD_withPrecon_RandomVideo_8K7030_marketing_011911_Run1min46sec_Ramp0_ml_all.csv', 'WL_800GB/D2D_INTEL_SSD_800GB_BTWL3050037A800JGN_DL10_SSD_StandardD_withPrecon_RandomVideo_8K7030_marketing_011911_Run1min46sec_Ramp0_ml_all.csv', 'WL_800GB/D2D_INTEL_SSD_800GB_BTWL305003BQ800JGN_DL10_SSD_StandardD_withPrecon_RandomVideo_8K7030_marketing_011911_Run1min46sec_Ramp0_ml_all.csv', 'WL_800GB/D2D_INTEL_SSD_800GB_BTWL3094008V800MGN_DL10_SSD_StandardD_withPrecon_RandomVideo_8K7030_marketing_011911_Run1min46sec_Ramp0_ml_all.csv', 'WL_800GB/D2D_INTEL_SSD_800GB_BTWL3094009Y800MGN_DL10_SSD_StandardD_withPrecon_RandomVideo_8K7030_marketing_011911_Run1min46sec_Ramp0_ml_all.csv', 'WL_800GB/D2D_INTEL_SSD_800GB_BTWL309400A1800MGN_DL10_SSD_StandardD_withPrecon_RandomVideo_8K7030_marketing_011911_Run1min46sec_Ramp0_ml_all.csv']
pts=io.parse(fis)
datas=map(lambda x:x.data,pts)
d=datas[1]
c=map(lambda x:x[1],d)

countingdict={}
for i in c[1:]:
    if i=='':
        pass
    elif i in countingdict.keys():
        countingdict[i]+=1
    else: 
        countingdict[i]=1

sd=sorted(countingdict, key=countingdict.get)

for i in sd:
    print countingdict[i],':', i
