# #!/usr/bin/env python

import matplotlib.pyplot as plt
from numpy import arange

# Borrowed heavily from 
# http://matplotlib.org/1.2.1/examples/pylab_examples/multiple_yaxis_with_spines.html
# http://matplotlib.org/1.2.1/examples/index.html
# http://matplotlib.org/1.2.1/gallery.html
class  PlotPublisher():
	host=None 
	lines=[]
	xmin=None
	xmax=None
	ymin=None
	ymax=None

	def __init__(self, xlabel='', ylabel=''):
		fig = plt.figure()
		fig.subplots_adjust(right=0.75)
		self.host = fig.add_subplot(111)
		self.host.set_xlabel(xlabel)
		self.host.set_ylabel(ylabel)

	def addline(self, data, marker='', label='', color='black'):
		try:
			data=sorted(data)
		except:
			pass
	 	x=map(lambda xx:xx[0],data)
 		y=map(lambda xx:xx[1],data)

 		if all(map(lambda xx:isinstance(xx,str), x)):
 			newx=map(lambda xx:xx+.5,range(len(x)))
			plt.xticks(newx, tuple(map(str,x)))
	 		x=newx

 		if all(map(lambda xx:isinstance(xx,str), y)):
 			newy=map(lambda xx:xx+.5,range(len(y)))
			plt.yticks(newy, tuple(map(str,y)))
		 	y=newy
		 	

 		p = self.host.plot(x, y, 'o-', marker=marker, label=label, color=color)[0]
 		self.lines.append(p)

 		try:
 			if self.xmax==None:
 				self.xmax=max(x)+.1*abs(max(x))
	 		elif max(x) > self.xmax:
	 			self.xmax=max(x)+.1*abs(max(x))

	 		if self.xmin==None:
 				self.xmin=min(x)+.1*abs(min(x))
	 		if min(x) < self.xmin:
	 			self.xmin=min(x)-.1*abs(min(x))

	 		if self.ymax==None:
 				self.ymax=max(y)+.1*abs(max(y))
	 		if max(y) > self.ymax:
	 			self.ymax=max(y)+.1*abs(max(y))

			if self.ymin==None:
 				self.ymin=min(y)+.1*abs(min(y))	 		
	 		if min(y) < self.ymin:
	 			self.ymin=min(y)-.1*abs(min(y))
	 	except:
	 		pass

		self.host.set_xlim(self.xmin, self.xmax)
		self.host.set_ylim(self.ymin, self.ymax)
		self.host.legend(self.lines, map(lambda x:x.get_label(), self.lines))
		return p	

	def aaaddline(self, data, marker='', label='', color='black'):
		try:
			data=sorted(data)
		except:
			pass
	 	x=map(lambda x:x[0],data)
 		y=map(lambda x:x[1],data)
 		p = self.host.plot(x, y, 'o-', marker=marker, label=label, color=color)[0]
 		self.lines.append(p)

 		try:
	 		if max(x) > self.xmax:
	 			self.xmax=max(x)+.1*max(x)
	 		if min(x) < self.xmin:
	 			self.xmin=min(x)-.1*min(x)
	 		if max(y) > self.ymax:
	 			self.ymax=max(y)+.1*max(y)
	 		if min(y) < self.ymin:
	 			self.ymin=min(y)-.1*min(y)
	 	except:
	 		pass
	 	
		self.host.set_xlim(self.xmin, self.xmax)
		self.host.set_ylim(self.ymin, self.ymax)
		self.host.legend(self.lines, map(lambda x:x.get_label(), self.lines))
		return p

	def show(self):
		plt.show()

if __name__=="__main__":
	pub=PlotPublisher('xs','ys')
	pub.addline([(1,9), (2,7), (3,8)], marker='1', label='a', color='blue')
	pub.addline([('this',9), ('that',7), ('theother',8)], marker='1', label='a', color='blue')
	pub.addline([('moose',9), ('mouse',7), ('muskox',8)], marker='1', label='a', color='blue')
	pub.addline([(9,'this'), (7,'that'), (8,'theother')], marker='1', label='a', color='blue')
	pub.show()



# from matplotlib import pyplot
# from matplotlib.lines import Line2D

# def plot(data, marker=None, color='b', markersize=10, debug=0):
# 	if debug==2: 
# 		print marker

# 	data=sorted(data)
# 	x=map(lambda x:x[0],data)
# 	y=map(lambda x:x[1],data)

# 	fig = pyplot.figure()
# 	ax = fig.add_subplot(1,1,1)
# 	ax.plot(x, y, 'o-',marker=marker, color=color, markersize=markersize)
# 	pyplot.show()

# if __name__=="__main__":
# 	markers=['0', '1', '2', '3', '4', 'D', '6', '7', 's', '|', 'x', '5', '_', '^', 'd', 'h', '+', '*', ',', 'o', '.', '1', 'p', '3', '2', '4', 'H', 'v', '8', '<', '>', r'$\lambda$', r'$\bowtie$', r'$\circlearrowleft$',r'$\clubsuit$',r'$\checkmarks$']
# 	from random import randrange
# 	plot([(1,2),(3,4),(5,6),(3.1,5)], marker=markers[randrange(len(markers))], color='b', markersize=20, debug=2)
# 	plot([(2,1),(4,3),(6,4),(5,3.1)], marker=markers[randrange(len(markers))], color='b', markersize=20, debug=2)
