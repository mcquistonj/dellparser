#!/usr/bin/env python
"""Creates xls file from a testresult object"""
from testresult import *
from matplotlib.pyplot import show, figure, barh, yticks, title, grid, savefig
from numpy import arange

class GraphPublisher():
    def publish(self, parsedOutput, outputfile='', debug=0, argsdict={}):
        self.DEBUG=debug
        self.PLOTS=1
        datas=map(lambda x:x.data,parsedOutput)
        d=datas[1]
        c=map(lambda x:x[1],d)
        countingdict={}
        for it in c[1:]:
            i = it.replace('%',' ')
            if i=='':
                pass
            elif i in countingdict.keys():
                countingdict[i]+=1
            else: 
                countingdict[i]=1
        sd=sorted(countingdict, key=countingdict.get)
        if self.DEBUG==2:
            for i in sd:
                print countingdict[i],':', i
        showallatonce=True
        self.makefig(countingdict.keys(), countingdict.values(), 'Access specification occurances', 'Occurances', not(showallatonce))
        if showallatonce:
            show()

    def makefig(self, keys, values, keylabel='', valuelabel='', show=True):
        val = values
        # the bar lengths
        pos = arange(len(keys))+.5
        # the bar centers on the y axis
        fig=figure(self.PLOTS)

        self.PLOTS+=1
        a=barh(pos,val, align='center')
        # print str(a)
        yticks(pos, tuple(keys),size='x-small')
        title(keylabel)
        
        grid(True)
        self.labels=keys
        fig.subplots_adjust(left=.5)
        if show:
            show()
        savefig('figure{N}.png'.format(N=self.PLOTS-1))

