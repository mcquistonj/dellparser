cd ~/Downloads/
rm dellparser*.zip
git clone git@bitbucket.org:mattcarlson/dellparser.git

rm -rf dellparser/Excel_macros/ dellparser/scripts/ dellparser/testcode/ dellparser/examplefiles/ dellparser/.git/ dellparser/.gitignore dellparser/*.xls* dellparser/*.png
zip -r -q dellparser.zip dellparser
rm -rf dellparser

mv dellparser.zip "dellparser.`date +%m%d%Y`.`date +%H%M`.zip"
