cd ~/Downloads/
rm dellparser*.zip
git clone git@bitbucket.org:mattcarlson/dellparser.git
vboxmanage startvm WinBox
VBoxManage guestcontrol WinBox execute --image "C:\Users\mcarlson\Desktop\dellparserbuild.bat" --username mcarlson --password testlabs --verbose --wait-stdout --wait-exit
VBoxManage controlvm WinBox savestate
mv dellparser.zip "dellparser.`date +%m%d%Y`.`date +%H%M`.zip"
