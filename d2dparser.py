#!/usr/bin/python
from testresult import *
from datetime import datetime, timedelta, date
from os import rename
from sys import stdout
import glob

class D2DParser():
    def parse(self, filenames, debug=0, configs=None):
        self.DEBUG=debug
        perfs = self.genPerfFiltered(filenames, debug, configs)
        # self.rename(filenames)
        iops=self.geniops(perfs)
        mbps=self.genmbps(perfs)
        return perfs+[iops,mbps]

    def rename(self, filenames, dryrun=True):
        rename=[]
        script=''
        for filename in filenames:
            with open(filename) as log:
                for line in log:
                    if line.strip().endswith('Test Description'):
                        line=log.next()
                        script = line[line.find(',')+1:]
                        break
            datestr='%m%d%y'
            td=date.today()
            cap='{CAP}'
            sn='{SN}'
            fw='{FW}'            
            newname='D2D_Intel_SSD_{CAP}_{SN}_{FW}_{script}_{mmddyy}.csv'.format(CAP=cap, SN=sn, FW=fw, script=script, mmddyy=td.strftime(datestr))
            prefix=filename[:max(filename.rfind('/'),filename.rfind('\\'))+1]
            rename.append((prefix,filename,newname))
        if dryrun:
            if self.DEBUG==2:
                print 'Renaming (dryrun):\n'
                for prefix,old,new in rename:
                    print old, '\n -> \n', prefix+new
                    print
        else:
            for prefix, old, new in rename:
                os.rename(old,prefix+new)
        

    def geniops(self, pages):
        test = ParsedTest()
        test.name = 'Iops'
        test.files=None
        test.data=[]

        for i in xrange(len(pages)):
            if i==0:
                for row in pages[i].data:
                    test.data.append(row[:3])
            else:
                for j in xrange(min(len(test.data),len(pages[i].data))):
                    test.data[j].append(pages[i].data[j][2])
        for row in test.data:
            if row[0]=='Queue Depth':
                row.append('MAX')
                row.append('MIN')
                row.append('D2D Compare')
            elif row[0]=='':
                row.append('')
                row.append('')
                row.append('')
            else:
                nums=map(float,row[2:])
                maximum=max(nums)
                minimum=min(nums)
                D2D=self.d2dformatter(maximum,minimum)
                row.append(str(maximum))
                row.append(str(minimum))
                row.append(D2D)
        return test

    def d2dformatter(self, maximum, minimum):        
        rawd2d=abs((minimum-maximum)/maximum*100)
        if rawd2d>15:
            return "FAILEDLEVEL3:{0:.2f}%".format(rawd2d)
        if rawd2d>10:
            return "FAILEDLEVEL2:{0:.2f}%".format(rawd2d)
        if rawd2d>5:
            return "FAILEDLEVEL1:{0:.2f}%".format(rawd2d)
        else:
            return "{0:.2f}%".format(rawd2d)

    def genmbps(self, pages):
        test = ParsedTest()
        test.name = 'Mbps'
        test.files=None
        test.data=[]
        for i in xrange(len(pages)):
            if i==0:
                for row in pages[i].data:
                    test.data.append(row[:2]+[row[3]])
            else:
                for j in xrange(min(len(test.data),len(pages[i].data))):
                    test.data[j].append(pages[i].data[j][3])
        for row in test.data:
            if row[0]=='Queue Depth':
                row.append('MAX')
                row.append('MIN')
                row.append('D2D Compare')
            elif row[0]=='':
                row.append('')
                row.append('')
                row.append('')
            else:
                nums=map(float,row[2:])
                maximum=max(nums)
                minimum=min(nums)
                D2D=self.d2dformatter(maximum,minimum)
                row.append(str(maximum))
                row.append(str(minimum))
                row.append(D2D)
        return test
 
    def genPerfFiltered(self, filenames, debug=0, configs=None):        
        tests = []
        if self.DEBUG==1: self.loops=0
        if configs and 'iometer' in configs.keys():
            filenames=self.flatten(map(glob.glob, configs['iometer'].values()))
        datalocs=[44, 2, 6, 9, 14, 19, 24, 45]
        filenumber=1
        for filename in filenames:
            test = ParsedTest()
            if filenumber>1:
                test.name = 'PerfFilterd ({ix})'.format(ix=filenumber)
            else:
                test.name = 'PerfFilterd'
            filenumber+=1
            test.files=[filename]
            test.data=[['Queue Depth', 'Access Specification Name', 'IOps', 'MBps', 'Average Response Time', 'Maximum Response Time', 
                'Errors', '% CPU Utilization']]
            with open(filename) as log:
                for line in log:
                    if line.startswith("'Time Stamp"):
                        line=log.next()
                        starttime = datetime.strptime(line.replace(',','').strip()[:-4], "%Y-%m-%d %H:%M:%S")
                    elif line.startswith("'Target Type"):
                        header=map(lambda x:x.strip(),line[1:].upper().split(','))
                        QDloc = header.index('QUEUE DEPTH')
                        ASNloc = header.index('ACCESS SPECIFICATION NAME')
                        IOPSloc = header.index('IOPS')
                        MBPSloc = header.index('MBPS')
                        ARTloc = header.index('AVERAGE RESPONSE TIME')
                        MRTloc = header.index('MAXIMUM RESPONSE TIME')
                        ERRloc = header.index('ERRORS')
                        CPUUloc = header.index('% CPU UTILIZATION')
                        datalocs = [QDloc, ASNloc, IOPSloc, MBPSloc, ARTloc, MRTloc, ERRloc, CPUUloc]
                        line=log.next()
                        vals=line.split(',')
                        if len(vals) > max(datalocs):
                            row=[]
                            shouldappend=True
                            for i in datalocs:
                                c=vals[i].strip()
                                if c:
                                    row.append(c)
                                else:
                                    shouldappend=False
                                    break
                            if shouldappend:
                                try:
                                    if int(row[0])<int(test.data[-1][0]):
                                        test.data.append(['','','','','','','',''])
                                except:
                                    pass
                                test.data.append(row)

            tests.append(test)
            if self.DEBUG==1: 
                self.loops+=1
                stdout.write("\r"+'.'*self.loops)
                stdout.flush()
        if self.DEBUG==1: print ''
        return tests

    def flatten(self,l):
        ret=[]
        for i in l:
            if isinstance(i,list):
                ret+=self.flatten(i)
            else:
                ret.append(i)
        return ret
