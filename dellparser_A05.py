#!/usr/bin/python

import os, sys, glob, argparse, sys
from testresult import *
from PySide import QtGui

from d2dparser import D2DParser
from d2dpublisher import D2DPublisher

from sixtythreehourparser import SixtyThreeHourParser
from sixtythreehourpublisher import SixtyThreeHourPublisher

from basicparser import BasicParser
from basicpublisher import BasicPublisher

from ldparser import LDParser
from ldpublisher import LDPublisher

__parserVersion = '0.0.1'
__scriptPath = os.path.dirname(os.path.realpath(sys.argv[0]))


class ListAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string):
        print 'Dell Parser version', parser.version
        sys.exit()


class DellGUI(QtGui.QWidget):
    OUTPUTFILE=''
    FILENAMES=''
    MODE=''

    def getresults(self):
        return (self.FILENAMES, self.OUTPUTFILE,  self.MODE)

    def __init__(self, app):
        super(DellGUI, self).__init__()
        caption = 'Open file'
        directory = './'
        filter_mask = "Comma Seperated Value sheets (*.csv)"
        self.FILENAMES = QtGui.QFileDialog.getOpenFileNames(None, caption, directory, filter_mask)[0]

        if arguments.output:
            self.OUTPUTFILE=arguments.output
            validinput=True
        else:
            self.OUTPUTFILE, validinput=QtGui.QInputDialog.getText(self, 'Input Dialog', 
                'Enter in the desired name for the output file:')
        if not validinput:
            self.OUTPUTFILE='Dellparser.xlsx'

        if arguments.mode[0] == 'BASIC':
            self.MODE='BASIC'
        elif arguments.mode[0] == 'D2D':
            self.MODE='D2D'
        elif arguments.mode[0] == '63HOUR':
            self.MODE='63HOUR'
        elif arguments.mode[0] == 'LD':
            self.MODE='LD'
        else:
            mode, validinput=QtGui.QInputDialog.getItem(self, 'Input Dialog', 
                'Enter in the parser mode:', ['Basic','D2D', '63 Hour', 'LD'], editable=True)
            if not validinput or mode=='Basic':
                self.MODE='Basic'
            elif mode=='D2D':
                self.MODE='D2D'
            elif mode=='LD':
                self.MODE='LD'
            else:
                self.MODE='63HOUR'

    def onActivated(self, text):
        self.lbl.setText(text)
        self.lbl.adjustSize()  

    def onactivate(self, args):
        print args

def flatten(l):
    ret=[]
    for i in l:
        if isinstance(i,list):
            ret+=flatten(i)
        else:
            ret.append(i)
    return ret

# Set up exit codes
EXIT_ALL_PASSED=0
EXIT_SOME_FAILURES=5522
EXIT_WARNINGS=0 # 5521
EXIT_CODE=EXIT_ALL_PASSED

# Configure argument parser for CLI invocation
argumentParser = argparse.ArgumentParser(
    description='EEV Test Parser',
    add_help=False, argument_default='./')

argumentParser.version = __parserVersion

optionalArguments = argumentParser.add_argument_group(
    title='Optional arguments')
optionalArguments.add_argument(
    '-m', '--mode', nargs=1, type=str,
    choices=['BASIC','D2D','63HOUR', 'LD'],
    required=False, default=[''], 
    help='The mode for the parser to parse the logs. '
    'Choose either "BASIC," "D2D," or "63HOUR."',
    metavar='mode', dest='mode')

optionalArguments.add_argument(
    'input', nargs='*', default=None, type=str, 
    help='The input log files to parse', metavar='inputLogFiles')

optionalArguments.add_argument(
    '-h', '--help', action='help', help='Show this help message and exit.')

optionalArguments.add_argument(
    '-o', '--output', default=None, type=str, 
    required=False, help='Desired name of the output file. Defaults to '
    '"Dellparser.xlsx"', metavar='output.file')

optionalArguments.add_argument(
    '-d', '--debug-level', nargs=1, default=[0],
    type=int, required=False,
    help='Specify a level of debugging text output to the console:'
    '0 - no debugging,'
    '1 - minimal debugging,'
    '2 - verbose debugging.',
    metavar='debuglevel', dest='debug')

# Patch for case-insensitive test tool lookup
for name in ['-m', '--mode']:
    try:
        i = sys.argv.index(name) + 1
        sys.argv[i] = sys.argv[i].upper().replace('63HR','63HOUR')
        break
    except:
        pass

arguments = argumentParser.parse_args()

DEBUG=arguments.debug[0]
# FILENAMES=[]
# OUTPUTFILE=''
# MODE='D2D'
##############################################################################
if arguments.input:
    FILENAMES=reduce(lambda a, b: a + b, map(glob.glob, arguments.input))
    if arguments.output:
        OUTPUTFILE=arguments.output
    else:
        OUTPUTFILE='Dellparser.xlsx'
    if arguments.mode:
        MODE=arguments.mode[0]
    else:
        MODE='BASIC'
else: 
    app = QtGui.QApplication([])
    gui = DellGUI(app)
    FILENAMES, OUTPUTFILE, MODE = gui.getresults()
##############################################################################

if MODE=='D2D':
    parser = D2DParser()
    publisher=D2DPublisher()
elif MODE=='63HOUR':
    parser = SixtyThreeHourParser()
    publisher=SixtyThreeHourPublisher()
elif MODE=='LD':
    parser = LDParser()
    publisher=LDPublisher()
else: #if mode == 'BASIC'
    if all(map(lambda x:x[max(x.find('/'),x.find('\\'))+1:][:2].upper()=='LD', FILENAMES)):
        parser = LDParser()
        publisher=LDPublisher()
    else:
        parser = BasicParser()
        publisher=BasicPublisher()

parsedOutput = parser.parse(FILENAMES, DEBUG)
publisher.publish(parsedOutput, 
                   outputfile=os.path.abspath(OUTPUTFILE), 
                   debug=DEBUG)
sys.exit(EXIT_CODE)