#!/usr/bin/env python
"""Creates xls file from a testresult object"""
from testresult import *
from xlsxwriter.workbook import Workbook

# https://xlsxwriter.readthedocs.org/en/latest/working_with_cell_notation.html

# https://xlsxwriter.readthedocs.org/en/latest/working_with_charts.html
# https://xlsxwriter.readthedocs.org/en/latest/examples.html
# https://xlsxwriter.readthedocs.org/en/latest/example_conditional_format.html
# https://xlsxwriter.readthedocs.org/en/latest/example_images.html
# https://xlsxwriter.readthedocs.org/en/latest/working_with_formats.html
# https://xlsxwriter.readthedocs.org/en/latest/working_with_conditional_formats.html
class D2DPublisher():
    def publish(self, parsedOutput, outputfile='D2D_Compare.xlsx', debug=0, 
                argsdict={}):
        self.DEBUG=debug
        if outputfile.endswith('.xlsx'):
            try: os.remove(outputfile)
            except: pass
            workbook = Workbook(outputfile)
        elif outputfile.endswith('.xls'):
            outputfile=outputfile+'x'
            try: os.remove(outputfile)
            except: pass
            workbook = Workbook(outputfile)
        elif outputfile.endswith('.XLSX'):
            try: os.remove(outputfile)
            except: pass
            workbook = Workbook(outputfile)
        elif outputfile.endswith('.XLS'):
            outputfile=outputfile+'X'
            try: os.remove(outputfile)
            except: pass
            workbook = Workbook(outputfile)
        else:
            try: os.remove(outputfile+'.xlsx')
            except: pass
            workbook = Workbook(outputfile+'.xlsx')

        failedstyle1 = workbook.add_format({'bg_color': '#993300'})
        failedstyle2 = workbook.add_format({'bg_color': '#FFCC00'})
        failedstyle3 = workbook.add_format({'bg_color': '#FFFF99'})
        mbpssheet=None
        iopssheet=None
        for p in parsedOutput:
            worksheet = workbook.add_worksheet(p.name)
            if p.name=='Mbps':
                mbpssheet=worksheet
            if p.name=='Iops':
                iopssheet=worksheet
            for y,row in enumerate(p.data):
                for x, val in enumerate(row+[' ']):
                    dat=val[val.find(':')+1:]
                    if val.startswith('FAILEDLEVEL1:'):
                        worksheet.write(y, x, dat, failedstyle3)
                    elif val.startswith('FAILEDLEVEL2:'):
                        worksheet.write(y, x, dat, failedstyle2)
                    elif val.startswith('FAILEDLEVEL3:'):
                        worksheet.write(y, x, dat, failedstyle1)
                    else:
                        worksheet.write(y, x
                            , dat)
                # if p.name in ['Iops','Mbps']:
                #     worksheet.write(y, x, row[0]+' '+row[1])
                #     worksheet.set_column(x, x, None, None, 
                #                          {'hidden': 1, 'collapsed': 1})
            # worksheet.protect() ############################################
            if p.name=='Iops':
                iopsdat=p.data[1:]
                iopsdat_loc=map(lambda x:x[0],filter(lambda x:x[1]=='IOps',enumerate(p.data[0])))
            if p.name=='Mbps':
                mbpsdat=p.data[1:]
                mbpsdat_loc=map(lambda x:x[0],filter(lambda x:x[1]=='MBps',enumerate(p.data[0])))
        QD_loc = 0 #p.data[0].index('Queue Depth')
        ASN_loc = 1 #p.data[0].index('Access Specification Name')
        end_col=x
        end_row=y
        workloads={}
        if len(mbpsdat)<len(iopsdat):
            data=mbpsdat
        else:
            data=iopsdat
        for i,line in enumerate(data):
            if not all(map(lambda x:x=='',line)):
                qd=line[QD_loc]
                asn=line[ASN_loc]
                mbpsline=map(lambda x:mbpsdat[i][x], mbpsdat_loc)
                iopsline=map(lambda x:iopsdat[i][x], iopsdat_loc)
                # workloads[asn][occurance][qd]
                if asn in workloads.keys():
                    occurance=len(workloads[asn])
                    if occurance in workloads[asn].keys():
                        if qd in workloads[asn][occurance].keys():
                            workloads[asn][occurance+1]={}
                            workloads[asn][occurance+1][qd]=iopsline+mbpsline
                        else:
                            workloads[asn][occurance][qd]=iopsline+mbpsline
                    else:
                        workloads[asn][occurance][qd]=iopsline+mbpsline
                else:
                    workloads[asn]={}
                    workloads[asn][1]={}
                    workloads[asn][1][qd]=iopsline+mbpsline

        header=['Queue Depth, Access Specification Name', 'Occurances'] + ['IOps']*len(iopsdat_loc) + ['MBps']*len(mbpsdat_loc)
        maxlen=len(header)
        worksheet.write_row(0, x, header)
        y=1
        AccessSpecdepths=[1]
        for asn, occuranceD in workloads.items():
            # print asn
            for occurance, qd_d in occuranceD.items():
                # print '\t', occurance
                for qd in sorted(map(int,qd_d)):
                    # print '\t\t', qd
                    # print '\t\t\t', qd_d[str(qd)]
                    
                    line=[' '.join([str(qd), asn, ])] + [str(occurance)] + qd_d[str(qd)]

                    worksheet.write_row(y, x, line)
                    maxlen=max(maxlen,2+len(qd_d[str(qd)]))
                    y+=1
                worksheet.write_row(y, x, ['']*maxlen)
                y+=1
                # worksheet.write_row(y, x, [y]+ dat)
            AccessSpecdepths.append(y)
        if self.DEBUG<2:
            worksheet.set_column(x, x+maxlen, None, None, {'hidden': 1, 'collapsed': 1})

        mbpscharts=[]
        iopscharts=[]
        for lh,rh in zip(AccessSpecdepths[:-1],AccessSpecdepths[1:]):
            mbchart = workbook.add_chart({'type': 'line', 'subtype': 'with_markers'})
            iochart = workbook.add_chart({'type': 'line', 'subtype': 'with_markers'})
            for j, col in enumerate(range(x+1,x+maxlen)):
                if j==0:
                    pass
                # chart.add_series({'values': ['Mbps', lh, col, rh, col], 
                #                   'categories': ['Mbps', lh, x, rh, x],
                #                   'name': 'Occurances'})
                elif j<len(iopsdat_loc)+1:
                    iochart.add_series({'values': ['Mbps', lh, col, rh-2, col], 
                                      'categories': ['Mbps', lh, x, rh-2, x],
                                      'name': 'IOps '+str(j)})
                else:
                    mbchart.add_series({'values': ['Mbps', lh, col, rh-2, col], 
                                      'categories': ['Mbps', lh, x, rh-2, x],
                                      'name': 'MBps '+str(j-len(iopsdat_loc))})
            mbchart.show_hidden_data()
            iochart.show_hidden_data()
            mbpscharts.append(mbchart)
            iopscharts.append(iochart)

        for i, chart in enumerate(mbpscharts):
            mbpssheet.insert_chart(20*i+1, x+maxlen+2, chart)
        for i, chart in enumerate(iopscharts):
            iopssheet.insert_chart(20*i+1, x+2, chart)
        
        # chart = workbook.add_chart({'type': 'line', 'subtype': 'with_markers'})
        # for j, col in enumerate(range(x+1,x+maxlen)):
        #     if j==0:
        #         chart.add_series({'values': ['Mbps', 1, col, y, col], 
        #                           'categories': ['Mbps', 1, x, y, x],
        #                           'name': 'Occurances'})
        #     elif j<len(iopsdat_loc)+1:
        #         chart.add_series({'values': ['Mbps', 1, col, y, col], 
        #                           'categories': ['Mbps', 1, x, y, x],
        #                           'name': 'IOps '+str(j)})
        #     else:
        #         chart.add_series({'values': ['Mbps', 1, col, y, col], 
        #                           'categories': ['Mbps', 1, x, y, x],
        #                           'name': 'MBps '+str(j-len(iopsdat_loc))})
        # chart.show_hidden_data()
        # worksheet.insert_chart(1, x+5, chart)

        workbook.close()

        # countingdict={}
        # outputdict={}
        # for i in xrange(min(len(iopsdat),len(mbpsdat))):
        # # for line in p.data:
        #     line=mbpsdat[i]

        #     key=line[QD_loc]+'->'+line[ASN_loc]
        #     if key in countingdict.keys():
        #         countingdict[key]+=1
        #     else:
        #         countingdict[key]=1
        #     mbpsline=map(lambda x:mbpsdat[x], mbpsdat_loc)
        #     iopsline=map(lambda x:iopsdat[x], iopsdat_loc)

        #     outputdict[key+'->'+str(countingdict[key])]=iopsline+mbpsline
        # for k,v in outputdict.items():
        #     print k, '\n\t', v
        #     print 
            # if line[ASNloc] and line[ASNloc]!='Access Specification Name' and line[ASNloc] in accdict.keys():
            #     accdict[line[ASNloc]].append(line[ASNloc+1:MAXloc])
            # elif line[ASNloc] and line[ASNloc]!='Access Specification Name':
            #     accdict[line[ASNloc]]=[line[ASNloc+1:MAXloc]]
        # if False:
        #     if False and p.name=='Iops':
        #         ASNloc=p.data[0].index('Access Specification Name')
        #         iopsloc=map(lambda x:x[0],
        #                     filter(lambda x:x[1]=='IOps', 
        #                            list(enumerate(p.data[0]))))
        #         iopschart = workbook.add_chart({'type': 'line', 
        #                                     'subtype': 'with_markers'})
        #         for io in iopsloc: ##########################################################################################################################################################################
        #             iopschart.add_series({'values': ['Iops', 1, io, y, io], 
        #                               'categories': ['Iops', 1, x, y, x],
        #                               'name': p.data[0][io] })
        #             iopschart.show_hidden_data()
        #         # worksheet.insert_chart(1, x+5, iopschart)

        #     if False and p.name=='Iops':
        #         accdict={}
        #         ASNloc=p.data[0].index('Access Specification Name')
        #         MAXloc=p.data[0].index('MAX')
        #         for line in p.data:
        #             if line[ASNloc] and line[ASNloc]!='Access Specification Name' and line[ASNloc] in accdict.keys():
        #                 accdict[line[ASNloc]].append(line[ASNloc+1:MAXloc])
        #             elif line[ASNloc] and line[ASNloc]!='Access Specification Name':
        #                 accdict[line[ASNloc]]=[line[ASNloc+1:MAXloc]]
        #         charts=[]
        #         x+=1

        #         for key, data in accdict.items():
        #             for y, dat in enumerate(data):
        #                 worksheet.write_row(y, x, [y]+ dat)
        #             chart = workbook.add_chart({'type': 'line',
        #                                         'subtype': 'with_markers'})
        #             chart.set_title({'name': key})
        #             for col in range(x+1,x+len(dat)):
        #                 chart.add_series({'values': ['Iops', 0, col, len(data),
        #                                               col],
        #                                   'categories' : ['Iops', 0, x, 
        #                                                   len(data), x]})
        #             worksheet.set_column(x, x+len(dat), None, None, 
        #                                  {'hidden': 1, 'collapsed': 1})
        #             chart.show_hidden_data()
        #             charts.append(chart)
        #             x=x+len(dat)+1

        #         for i,chart in enumerate(charts):
        #             worksheet.insert_chart(i*20+40, x+5, chart)
        #         worksheet.insert_chart(1, x+5, iopschart)

        #     if False and p.name=='Mbps':
        #         ASNloc=p.data[0].index('Access Specification Name')
        #         iopsloc=map(lambda x:x[0], 
        #                     filter(lambda x:x[1]=='MBps',
        #                            list(enumerate(p.data[0]))))
        #         mbpschart = workbook.add_chart({'type': 'line', 
        #                                         'subtype': 'with_markers'})
        #         for io in iopsloc: ##########################################################################################################################################################################
        #             mbpschart.add_series({'values': ['Mbps', 1, io, y, io], 
        #                                   'categories' : ['Mbps', 1, x, y, x],
        #                                   'name': p.data[0][io]})
        #             mbpschart.show_hidden_data()
        #     if False and p.name=='Mbps':
        #         countingdict={}
        #         ASNloc=p.data[0].index('Access Specification Name')
        #         for line in p.data:
        #             if line[ASNloc] and line[ASNloc]!='Access Specification Name' and line[ASNloc] in countingdict.keys():
        #                 countingdict[line[ASNloc]]+=1
        #             elif line[ASNloc] and line[ASNloc]!='Access Specification Name':
        #                 countingdict[line[ASNloc]]=1
        #         worksheet.write_row(0, x+1, ['Access Specification', 
        #                                 'Occurances of Access Specification'])
        #         for y, (acc, count) in enumerate(countingdict.items()):
        #             worksheet.write_row(y+1, x+1, [acc, count])
        #         worksheet.set_column(x+1, x+2, None, None, 
        #                              {'hidden': 1, 'collapsed': 1})
        #         acnchart = workbook.add_chart({'type': 'column'})
        #         acnchart.add_series({'categories': ['Mbps', 1, x+1, y, x+1],
        #                        'values': ['Mbps', 1, x+2, y, x+2],
        #                       'name': 'Occurances of Access Specification'})
        #         acnchart.show_hidden_data()
        #         x+=3
        #     if False and p.name=='Mbps':
        #         accdict={}
        #         ASNloc=p.data[0].index('Access Specification Name')
        #         MAXloc=p.data[0].index('MAX')
        #         for line in p.data:
        #             if line[ASNloc] and line[ASNloc]!='Access Specification Name' and line[ASNloc] in accdict.keys():
        #                 accdict[line[ASNloc]].append(line[ASNloc+1:MAXloc])
        #             elif line[ASNloc] and line[ASNloc]!='Access Specification Name':
        #                 accdict[line[ASNloc]]=[line[ASNloc+1:MAXloc]]
        #         charts=[]
        #         for key, data in accdict.items():
        #             for y, dat in enumerate(data):
        #                 worksheet.write_row(y, x, [y]+ dat)
        #             # print ['Mbps', 0, self.indexToCol(x+1), len(data), self.indexToCol(x+len(dat))]
        #             # print ['Mbps', 0, x+1, len(data), x+len(dat)]
        #             chart = workbook.add_chart({'type': 'line',
        #                                         'subtype': 'with_markers'})
        #             chart.set_title({'name': key})
        #             for col in range(x+1,x+len(dat)):
        #                 chart.add_series({'values': 
        #                             ['Mbps', 0, col, len(data), col],
        #                               'categories' : 
        #                             ['Mbps', 0, x, len(data), x]})
        #             worksheet.set_column(x, x+len(dat), None, None, 
        #                                  {'hidden': 1, 'collapsed': 1})
        #             chart.show_hidden_data()
        #             charts.append(chart)
        #             x=x+len(dat)+1

        #         for i,chart in enumerate(charts):
        #             worksheet.insert_chart(i*20+40, x+5, chart)
        #         worksheet.insert_chart(1, x+5, mbpschart)
        #         worksheet.insert_chart(20, x+5, acnchart)
