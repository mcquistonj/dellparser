from distutils.core import setup
import py2exe

from matplotlib.pyplot import show, figure, barh, yticks, title, grid, savefig
from matplotlib import get_py2exe_datafiles
from numpy import arange

#import matplotlib.backends.backend_tkagg
#matplotlib.use('GTkGgg')#TkAgg')
#setup(console=['Dellparser.py']#,
#	  data_files=matplotlib.get_py2exe_datafiles()
#)
#opts = {
#  'py2exe': { "includes" : ["matplotlib.backends.backend_tkagg"] }
#}

setup(console=['Dellparser_A05.py'],
      options={
               'py2exe': {
                          'packages' :  ['matplotlib'],
                          'dll_excludes': ['libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll',
                                         'libgdk_pixbuf-2.0-0.dll',
                                           
##                                         'libgtk-win32-2.0-0.dll',
##                                         'libglib-2.0-0.dll',
##                                         'libcairo-2.dll',
##                                         'libpango-1.0-0.dll',
##                                         'libpangowin32-1.0-0.dll',
##                                         'libpangocairo-1.0-0.dll',
##                                         'libglade-2.0-0.dll',
##                                         'libgmodule-2.0-0.dll',
##                                         'libgthread-2.0-0.dll',
                                         'QtGui4.dll', 'QtCore.dll',
                                         'QtCore4.dll'
                                        ],
                          }
                },
      data_files=get_py2exe_datafiles()
    )   
