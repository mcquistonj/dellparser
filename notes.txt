08/22/2013 14:000
    Removed color coding and "%" from standard deviation colomn in 63 Hour.

08/21/2013 10:47
    Chaged 63 Hour parser to use D2D formula to calculate F2F rather than
    standard deviation. Standard deviation is in its own seperate column.

08/16/2013 15:50
    Added conditional colouring to 63 Hour standard deviation colomn. It matches
    that of D2D (>5% is light, >10% is medium and >15% is dark).
    Changed the column header to "F2F Deviation."

08/15/2013 10:38
    Removed minimum and maximums from IOps and MBps pages of 63 Hour output.

08/14/2013 13:53
    Added LD parser. Can either be activated with the -m LD switch, via the 
    GUI, or by choosing 'basic' (Via either of those methods) and then giving 
    files that all start with 'LD' (case insensitive).

08/12/2013 12:00
    Madse -m/--mode options case insensitive.

08/12/2013 11:45
    Basic parser now has one page per file, named after the file (no IOps/MBps 
    summary pages).

08/12/2013 11:30
    63 hour now parses out Firmware from the files. If it's missing, it 
    prompts the user.

08/06/2013 17:00
    Created mode switches for choosing Basic, D2D or 63 Hour parser. The GUI 
    will prompt you to choose one of the three options. In the command line use
    either -m or --mode followed by either 'BASIC', 'D2D', or '63HR'. It will 
    default to 63 Hour
    Also, implimented 63 Hour test.

08/06/2013 17:00
    Fixed charts.

08/05/2013 11:31
    Combined all the charts into one.

08/02/2013 11:31
    Added the queue depth access specification plot.

08/01/2013 12:12
    Added plotting of IOps and MBps for each access spec occurances. Again, this relies on extra hidden columns that actually contain the appropriate 
    data. 

08/01/2013 12:12
    Added plotting of access spec occurances. It relies on two extra hidden 
    columns that give the specs and totals. 
    Looks like we're sticking with the the colours.
    Graphing is still disabled, and will probably removed since everything we want can be accompolished with Excel, I think.

07/31/2013 12:38
    Added xlsx support, thanks to the xlsxwriter lib (included in this 
    distribution). To avoid confusion, file extension enforcement is now
    in place. XLSX files will be forced to have .xlsx extension, and XLS
    will be .xls. If this is not how they are input, then the proper extension 
    will be appended (not changed).
    Colours are better, but I still don't think they look right.
    Graphing is still disabled.

07/31/2013 10:13
    Adding notes.txt to the project, which is essentially a changelog. Also 
    temporarily removing graphing for this build, meaning the only output will 
    be the Excel spreadsheet. Still working on conditional colouring and 
    graphing.
